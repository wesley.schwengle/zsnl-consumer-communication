# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from zsnl_communication_consumer import consumers


class TestConsumer:
    """Tests for the consumer classes"""

    @mock.patch(
        "zsnl_communication_consumer.consumers.AttachedToMessageHandler"
    )
    @mock.patch("zsnl_communication_consumer.consumers.EmailImportHandler")
    @mock.patch("zsnl_communication_consumer.consumers.EmailSendHandler")
    def test_email_import_consumer(
        self, send_handler, import_handler, attached_to_message_handler
    ):
        consumer = consumers.CommunicationConsumer.__new__(
            consumers.CommunicationConsumer
        )

        import_handler().routing_keys = ["aap", "noot", "mies"]
        send_handler().routing_keys = ["a", "b"]

        consumer.cqrs = "cqrs"

        consumer._register_routing()

        assert consumer._known_handlers == [
            import_handler("cqrs"),
            send_handler("cqrs"),
            attached_to_message_handler("cqrs"),
        ]
        assert consumer.routing_keys == ["aap", "noot", "mies", "a", "b"]
